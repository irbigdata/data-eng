#### Setup A Sample Postgres DB

```bash
docker run --name pg_local -p 5432:5432 -e POSTGRES_USER=irBigData -e POSTGRES_PASSWORD=Sepahram -e POSTGRES_DB=tutorial -d postgres:13.2-alpine
```



### Import Data





#### Instructions 

- **Create and Activate a Python Venv**

  ```bash
  $ python -m venv .venv
  ```

  Windows - Powershell: 

  ```bash
  .\.venv\Scripts\activate.ps1     
  ```

  Windows - CMD: 

  ```bash
  .\.venv\Scripts\activate.bat
  ```

  Linux :

  ```bash
  source .venv/bin/activate
  ```

  

- **Install Required Libraries :**

	```bash
	pip3 install -r requirements.txt
	```

- **Run** `data_generator.py` to generate some fake data

  - ```bash
    $ python .\data_generator.py
    $ mkdir data_lake
    $ mv people.csv data_lake
    ```

- **Initialize GE**

  - ```python
    great_expectations init
    ```

  - Watch the video 

- **Edit Expectation Suite** 

  - ```bash
    great_expectations suite edit people.validate
    ```

  - *remove* : ***quantile expectations*** 

  - *remove* : ***row count expectation***

  - Add Another Expectation ifyou need  : 

    - [Glossary of Expectations — great_expectations documentation](https://docs.greatexpectations.io/en/latest/reference/glossary_of_expectations.html)

  - Run All Cells

 - **Create A Checkpoint To run the Validation :**

    - ```bash
       great_expectations checkpoint new checkpoint_pv1 people.validate
      ```

- **Run Ckeckpoint :**  

  - ```bash
    great_expectations checkpoint run checkpoint_pv1
    ```

- **Create a Python Script :**

    - ```bash
        great_expectations --v3-api checkpoint script checkpoint_pv1
        python great_expectations/uncommitted/run_checkpoint_pv1.py
        ```

- **Run Above Command in Nifi Container**

    - first of all : `apt install python3 , python3-pip, dos2unix ,  python3-venv`

- **move generate_data.py and requirements.txt to `nifi/workspace`**

    - **or** move people.csv there for simplicity.

- **Edit Checkpoint File**

    - replace system.exit(1) with system.exit(0)

    - return a dict 

        ```python
        if not results["success"]:
            print('{"result":"fail"}')
            sys.exit(0)
        print('{"result":"pass"}')
        ```

        

- **use ExecuteStreamCommand To intgerate it to our Data Pipeline(Watch the Video)**



